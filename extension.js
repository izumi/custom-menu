
const Lang = imports.lang;
const St = imports.gi.St;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Shell = imports.gi.Shell;
//const Util = imports.misc.util;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Mainloop = imports.mainloop;
const Clutter = imports.gi.Clutter;

const mainPanel = imports.ui.main.panel;	// Not a class?

const MENU_ICON_SIZE = 20

const HomeDir = GLib.getenv("HOME");
const GadgetDir = HomeDir + "/gadgetry";
const NoteDir = HomeDir + "/memoranda/notes";

const AppList = ["tkman.desktop", "cerezo.desktop", "vlctv.desktop",];
const SubAppList = ["qfixmemo.desktop", "ipython.desktop", "umgqineko.desktop",
		    "gtkps.desktop", "tabcmd.desktop", "reminder.desktop",
		    "stockicons.desktop",
		    "xcolors.desktop", "xcolorsel.desktop",
		    "swaphexa.desktop", "gsrestart.desktop"];

const NoteList = [
	// [Label, Directory, Filer regex]
	["Git", NoteDir, /^git-(.*)$/],
	["Python", NoteDir, /^python-(.*)$/],
	["Soccer", NoteDir, /^cerezo-(.*)$/],
	["Gnome", GadgetDir + "/gui/privtray/notes", /^gnome-(.*)$/],
    ];

const MenuButton = new Lang.Class({
    Name: 'MenuButton',

    //Extends: PanelMenu.SystemStatusButton,
    Extends: PanelMenu.Button,

    _init: function() {
	this.parent(0.0, "folder");
	let item;

	this._appendAppsMenuItems(this.menu, AppList);

	item = new PopupMenu.PopupMenuItem(_("LookingGlass"));
	item.connect('activate',
			function() { Main.createLookingGlass().toggle(); });
	this.menu.addMenuItem(item);

	item = new PopupMenu.PopupSubMenuMenuItem(_("Misc Apps"));
	this._appendAppsMenuItems(item.menu, SubAppList);
	this.menu.addMenuItem(item);

	item = new PopupMenu.PopupSeparatorMenuItem();
	this.menu.addMenuItem(item);

	for (let j = 0; j < NoteList.length; j++) {
	    let note = NoteList[j];
	    item = this._createNoteSubMenu(note[0], note[1], note[2]);
	    if (item != null) {
		this.menu.addMenuItem(item);
	    }
	}

	// Do something after a while.
	Mainloop.timeout_add(2800, this._delayedInit);

	this._setMenuTitle("Custom");
    },

    _setMenuTitle: function(name) {
	// In order to register as an actor wrap the label with a container

        let box = new St.BoxLayout({ style_class: 'panel-status-menu-box' });
        let label = new St.Label({ style_class: 'custom-menu-label',
				    text: name,
				    y_expand: true,
				    y_align: Clutter.ActorAlign.CENTER,
				    });

        box.add_child(label);
        box.add_child(PopupMenu.arrowIcon(St.Side.BOTTOM));
        this.actor.add_actor(box);
    },

    _delayedInit: function() {
    },

    _appendAppsMenuItems: function(menu, applist) {
        let appSys = Shell.AppSystem.get_default();
	for (let j = 0; j < applist.length; j++) {
	    let appname = applist[j];
	    let app = appSys.lookup_app(appname);
	    if (app != null) {
		let item = this._createAppMenuItem(app);
		menu.addMenuItem(item);
	    }
	}
    },

    _createAppMenuItem: function(app) {
	let item = this._createMenuItem(app.get_name(),
				    app.create_icon_texture(MENU_ICON_SIZE));

	item.connect('activate', Lang.bind(this, this._runApp, app));

	return item;
    },

    _createMenuItem: function(label, icon) {
	let item;
	if (icon != null) {
	    item = new PopupMenu.PopupBaseMenuItem();
	    let bin = new St.Bin();
	    bin.set_child(icon);
	    item.actor.add_child(bin);
	    item.actor.add_child(new St.Label({ text: label }),
		    				{ expand: true });
	} else {
	    item = new PopupMenu.PopupMenuItem(label);
	}

	return item;
    },

    _runApp: function(item, ev, app) {
	app.activate_full(-1, ev.get_time());
    },

    _createNoteSubMenu: function(name, dir, regex) {
	let item = null;

	let dh = Gio.file_new_for_path(dir);
	let files = dh.enumerate_children('standard::name',
			Gio.FileQueryInfoFlags.NONE, null);
	let info;
	while ((info = files.next_file(null)) != null) {
	    let label = this._noteLabel(info, regex);
	    if (label != null) {
		if (item == null) {
		    item = new PopupMenu.PopupSubMenuMenuItem(_(name));
		}
		let subitem = new PopupMenu.PopupMenuItem(_(label));
		subitem.connect('activate', Lang.bind(this, this._show_note,
				    dir + "/" + info.get_name()));
		item.menu.addMenuItem(subitem);
	    }
	}
	files.close(null);

	return item;
    },

    _noteLabel: function(info, regex) {
	let label = null;

	if (info.get_file_type() == Gio.FileType.REGULAR) {
	    let fname = info.get_name();
	    let match = regex.exec(fname);
	    if (match != null && match.length > 0) {
		label = (match.length > 1)? match[1]: fname;
	    }
	}
	return label;
    },

    _show_note: function(item, ev, file) {
	spawnAt(["gvim", "-geometry", "80x60", file,]);
    },
})

let customMenu = null;

function enable() {
    customMenu = new MenuButton();
    Main.panel.addToStatusArea('custom-menu', customMenu, 0);
}

function disable() {
    customMenu.destroy();
    customMenu = null;
}

/*
 * Async spawn in specified directory.
 * @args: array
 * @dir: execution directory or null.
 */
function spawnAt(args) {
    let dir = null;
    if (arguments.length > 1) {
	dir = arguments[1];
    }

    try {
        GLib.spawn_async(dir, args, null,
                         GLib.SpawnFlags.SEARCH_PATH,
                         null, null);
    } catch (err) {
	// Borrowed from misc.util.js of gnome-shell
	let title = _("Execution of '%s' failed:").format(args[0]);
	let msg = err.message
        if (err.code == GLib.SpawnError.G_SPAWN_ERROR_NOENT) {
            msg = _("Command not found");
        } else {
	    // Strip unnecessary part from the error message.
            // Extract the message in the parentheses.
            // This pattern match may fail if the message is localized.
            msg = err.message.replace(/.*\((.+)\)/, '$1');
        }
	Main.notifyError(title, msg);
    }
}
